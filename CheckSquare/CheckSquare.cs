using HW_10;
using System;
using Xunit;

namespace Square.Test
{
    public class uTestSquare
    {
        [Theory]
        [InlineData(5, 78.5)]
        [InlineData(0, 0)]

        public void CheckSquare(float r, double expected)
        {
            Program program = new Program();

            var actual = Program.Square(r);

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void CheckSquareExceprion()
        {
            Program program = new Program();

            float r = -10; 
            double square = 3.14 * r * r;

            string exception = "�� �������� ����";
            Assert.IsType<ArgumentOutOfRangeException>(exception);
        }
    }
}
