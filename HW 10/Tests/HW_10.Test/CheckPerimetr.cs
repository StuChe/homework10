using HW_10;
using Xunit;

namespace Perimetr.Test
{
    public class uTestPerimetr
    {
        [Fact]
        public void CheckPerimetr()
        {

            Program program = new Program();

            float r = 20;
            float d = 2 * r;
            double expected = d * 3.14;


            var actual = Program.Perimetr(r, out d);
                

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CheckPerimetrZero()
        {

            Program program = new Program();

            float r = 0;
            float d = 2 * r;
            double expected = d * 3.14;


            var actual = Program.Perimetr(r, out d);


            Assert.Equal(expected, actual);
        }
    }
}
