﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_10
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Что вы хотите посчитать? Периметр - 1, Радиус окружности - 2, Площадь - 3");
            char choise = char.Parse(Console.ReadLine());
            float radius;
            float square;

            float diametr = 0;

            switch (choise)
            {
                case '1':
                    Console.WriteLine("Введите радиус круга: ");
                    radius = float.Parse(Console.ReadLine());
                    Console.WriteLine("Периметр равен " + Perimetr(radius, out diametr) + " " + diametr);
                    break;

                case '2':
                    Console.WriteLine("Введите площадь круга: ");
                    square = float.Parse(Console.ReadLine());
                    Console.WriteLine("Радиус круга равен " + Radius(square));
                    break;

                case '3':
                    Console.WriteLine("Введите радиус круга: ");
                    radius = float.Parse(Console.ReadLine());
                    Console.WriteLine("Площадь круга равна " + Square(radius));
                    break;
            }
            Console.ReadKey();
        }

        public static double Perimetr(float r, out float d)
        {
            d = 2 * r;
            double perimetr = d * 3.14;
            return perimetr;
        }
        public static double Radius(float s)
        {
            double radius = Math.Sqrt(s * 3.14);
            return radius;
        }
        public static double Square(float r)
        {
            try
            {
                double square = 3.14 * r * r;
                return square;
            }
            catch (Exception e)
            {
                throw new ArgumentOutOfRangeException("Не валидный ввод");
            }
        }
    }
}
