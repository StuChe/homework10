using NUnit.Framework;
using System;
using HW_10;

namespace Radius.Test
{
    public class nTestRadius
    {
        [TestCase (10, 5.6035702904487605d)]
        [TestCase(0, 0)]
        [TestCase(-10, 0)]
        public void CheckRadius(float s, double expected)
        {
                Program program = new Program();

                double actual = Math.Sqrt(s * 3.14);

                Assert.AreEqual(expected, actual);
        }
    }
}